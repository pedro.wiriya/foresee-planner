package miniproject.demo;

public class Input {
    private String nama;
    private String nilai;

    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNilai() {
        return nilai;
    }
    public void setNilai(String nilai) {
        this.nilai = nilai;
    }
}