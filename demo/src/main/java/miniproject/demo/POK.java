//Code made by Adrian Chandra Rachman - 1906399575

package miniproject.demo;

public class POK extends MataKuliah {

    public POK(double nilai, boolean lulus) {
        super("Pengantar Organisasi Komputer", nilai, new MataKuliah[]{new PSD(0, true)}, lulus, 3, "Ilkom");
    }
}