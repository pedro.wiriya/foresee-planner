//Code made by Adrian Chandra Rachman - 1906399575
package miniproject.demo;

public class MPKAgama extends MataKuliah {

    public MPKAgama(double nilai, boolean lulus) {
        super("MPK Agama", nilai, new MataKuliah[]{}, lulus, 2, "SI/Ilkom");
    }
}