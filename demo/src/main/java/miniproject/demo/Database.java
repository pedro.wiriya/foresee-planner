//Code made by Adrian Chandra Rachman - 1906399575

package miniproject.demo;

import java.util.*;

public class Database {

    public static ArrayList<MataKuliah> databaseMatkulUser;
    private ArrayList<MataKuliah> ilkom;
    private ArrayList<MataKuliah> si;
    private ArrayList<MataKuliah> databaseMatkul;
    private static float ipk;
    private int sks;
    private ArrayList<String> matkul;

    public Database() {
        this.databaseMatkulUser = new ArrayList<MataKuliah>();
        this.databaseMatkul = new ArrayList<MataKuliah>();
        ilkom = new ArrayList<MataKuliah>();
        si = new ArrayList<MataKuliah>();
        this.matkul = new ArrayList<String>();
        createMatkulDatabase();
    }

    private void createMatkulDatabase() {
        this.databaseMatkul.add(new Adbis(0, false));
        this.databaseMatkul.add(new Alin(0, false));
        this.databaseMatkul.add(new BasDat(0, false));
        this.databaseMatkul.add(new DDAK(0, false));
        this.databaseMatkul.add(new DDP1(0, false));
        this.databaseMatkul.add(new DDP2(0, false));
        this.databaseMatkul.add(new FisDas(0, false));
        this.databaseMatkul.add(new MatDas(0, false));
        this.databaseMatkul.add(new Matdas2(0, false));
        this.databaseMatkul.add(new MD1(0, false));
        this.databaseMatkul.add(new MD2(0, false));
        this.databaseMatkul.add(new MPKAgama(0, false));
        this.databaseMatkul.add(new MPKING(0, false));
        this.databaseMatkul.add(new MPKOS(0, false));
        this.databaseMatkul.add(new MPKTA(0, false));
        this.databaseMatkul.add(new MPKTB(0, false));
        this.databaseMatkul.add(new OSIK(0, false));
        this.databaseMatkul.add(new OSSI(0, false));
        this.databaseMatkul.add(new PemLan(0, false));
        this.databaseMatkul.add(new POK(0, false));
        this.databaseMatkul.add(new PPM(0, false));
        this.databaseMatkul.add(new PPSI(0, false));
        this.databaseMatkul.add(new PPW(0, false));
        this.databaseMatkul.add(new PSD(0, false));
        this.databaseMatkul.add(new SDA(0, false));
        this.databaseMatkul.add(new SIAK(0, false));
        this.databaseMatkul.add(new SSP(0, false));
        this.databaseMatkul.add(new StatProb(0, false));
        this.databaseMatkul.add(new TBA(0, false));
        ilkom.add(new DDP1(0, false));
        ilkom.add(new FisDas(0, false));
        ilkom.add(new MD1(0, false));
        ilkom.add(new MPKING(0, false));
        ilkom.add(new MPKOS(0, false));
        ilkom.add(new MPKTB(0, false));
        ilkom.add(new DDP2(0, false));
        ilkom.add(new PSD(0, false));
        ilkom.add(new MD2(0, false));
        ilkom.add(new MatDas(0, false));
        ilkom.add(new MPKTA(0, false));
        ilkom.add(new Matdas2(0, false));
        ilkom.add(new SDA(0, false));
        ilkom.add(new PPW(0, false));
        ilkom.add(new POK(0, false));
        ilkom.add(new Alin(0, false));
        ilkom.add(new MPKAgama(0, false));
        ilkom.add(new BasDat(0, false));
        ilkom.add(new StatProb(0, false));
        ilkom.add(new PemLan(0, false));
        ilkom.add(new TBA(0, false));
        ilkom.add(new OSIK(0, false));
        si.add(new DDP1(0, false));
        si.add(new MatDas(0, false));
        si.add(new FisDas(0, false));
        si.add(new MD1(0, false));
        si.add(new MPKTB(0, false));
        si.add(new DDP2(0, false));
        si.add(new MD2(0, false));
        si.add(new MPKTA(0, false));
        si.add(new PPSI(0, false));
        si.add(new MPKING(0, false));
        si.add(new MPKOS(0, false));
        si.add(new PPM(0, false));
        si.add(new SDA(0, false));
        si.add(new PPW(0, false));
        si.add(new DDAK(0, false));
        si.add(new MPKAgama(0, false));
        si.add(new Adbis(0, false));
        si.add(new Alin(0, false));
        si.add(new StatProb(0, false));
        si.add(new SIAK(0, false));
        si.add(new SSP(0, false));
        si.add(new BasDat(0, false));

    }

    public void addUserMatkul(String[] data) {
            String matkul = data[0];
            String nilai = data[1];
            if (matkul.equalsIgnoreCase("adbis") || matkul.equalsIgnoreCase("administrasi bisnis")) {
                Database.databaseMatkulUser.add(new Adbis(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("alin") || matkul.equalsIgnoreCase("aljabar linear")) {
                Database.databaseMatkulUser.add(new Alin(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("basdat") || matkul.equalsIgnoreCase("basis data")) {
                Database.databaseMatkulUser.add(new BasDat(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ddak") || matkul.equalsIgnoreCase("dasar-dasar arsitektur komputer")) {
                Database.databaseMatkulUser.add(new DDAK(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ddp1") || matkul.equalsIgnoreCase("dasar-dasar pemrograman 1")) {
                Database.databaseMatkulUser.add(new DDP1(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ddp2") || matkul.equalsIgnoreCase("dasar-dasar pemrograman 2")) {
                Database.databaseMatkulUser.add(new DDP2(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("fisdas") || matkul.equalsIgnoreCase("fisika dasar")) {
                Database.databaseMatkulUser.add(new FisDas(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("matdas1") || matkul.equalsIgnoreCase("matematika dasar 1")) {
                Database.databaseMatkulUser.add(new MatDas(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("matdas2") || matkul.equalsIgnoreCase("matematika dasar 2")) {
                Database.databaseMatkulUser.add(new Matdas2(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("md1") || matkul.equalsIgnoreCase("matematika diskret 1")) {
                Database.databaseMatkulUser.add(new MD1(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("md2") || matkul.equalsIgnoreCase("matematika diskret 2")) {
                Database.databaseMatkulUser.add(new MD2(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("mpkagama") || matkul.equalsIgnoreCase("mpk agama")) {
                Database.databaseMatkulUser.add(new MPKAgama(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("mpking") || matkul.equalsIgnoreCase("mpk bahasa inggris")) {
                Database.databaseMatkulUser.add(new MPKING(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("mpkos") || matkul.equalsIgnoreCase("mpk olahraga & seni")) {
                Database.databaseMatkulUser.add(new MPKOS(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("mpkta") || matkul.equalsIgnoreCase("mpk terintegrasi a")) {
                Database.databaseMatkulUser.add(new MPKTA(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("mpktb") || matkul.equalsIgnoreCase("mpk terintegrasi b")) {
                Database.databaseMatkulUser.add(new MPKTB(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("osik") || matkul.equalsIgnoreCase("sistem operasi") || matkul.equalsIgnoreCase("os")) {
                Database.databaseMatkulUser.add(new OSIK(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ossi") || matkul.equalsIgnoreCase("sistem operasi") || matkul.equalsIgnoreCase("os")) {
                Database.databaseMatkulUser.add(new OSSI(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("pemlan") || matkul.equalsIgnoreCase("pemrograman lanjut")) {
                Database.databaseMatkulUser.add(new PemLan(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("pok") || matkul.equalsIgnoreCase("pengantar organisasi komputer")) {
                Database.databaseMatkulUser.add(new POK(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ppm") || matkul.equalsIgnoreCase("prinsip-prinsip manajemen")) {
                Database.databaseMatkulUser.add(new PPM(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ppsi") || matkul.equalsIgnoreCase("prinsip-prinsip sistem informasi")) {
                Database.databaseMatkulUser.add(new PPSI(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ppw") || matkul.equalsIgnoreCase("perancangan & pemrograman web")) {
                Database.databaseMatkulUser.add(new PPW(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("psd") || matkul.equalsIgnoreCase("pengantar sistem digital")) {
                Database.databaseMatkulUser.add(new PSD(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("sda") || matkul.equalsIgnoreCase("struktur data & algoritma")) {
                Database.databaseMatkulUser.add(new SDA(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("siak") || matkul.equalsIgnoreCase("sistem informasi akuntansi & keuangan")) {
                Database.databaseMatkulUser.add(new SIAK(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("ssp") || matkul.equalsIgnoreCase("sistem-sistem perusahaan")) {
                Database.databaseMatkulUser.add(new SSP(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("statprob") || matkul.equalsIgnoreCase("statistika & probabilitas")) {
                Database.databaseMatkulUser.add(new StatProb(cekNilai(nilai), cekLulus(nilai)));
            } else if (matkul.equalsIgnoreCase("tba") || matkul.equalsIgnoreCase("teori bahasa & automata")) {
                Database.databaseMatkulUser.add(new TBA(cekNilai(nilai), cekLulus(nilai)));
            }
        
    }
    public ArrayList<MataKuliah> getIlkom() {
        return ilkom;
    }
    public ArrayList<MataKuliah> getSi() {
        return si;
    }

    public MataKuliah getSpecificMatkul(String matkul) {
        for (MataKuliah mk : databaseMatkul) {
            if (mk.getNama().equalsIgnoreCase(matkul))
                return mk;
        }
        return null;
    }

    public static boolean cekLulus(String nilai) {
        if (cekNilai(nilai) < 2.0)
            return false;
        return true;
    }

    public static Double cekNilai(String nilai) {
        if (nilai.equalsIgnoreCase("a")) {
            return 4.0;
        } else if (nilai.equalsIgnoreCase("a-")) {
            return 3.7;
        } else if (nilai.equalsIgnoreCase("b+")) {
            return 3.3;
        } else if (nilai.equalsIgnoreCase("b")) {
            return 3.0;
        } else if (nilai.equalsIgnoreCase("b-")) {
            return 2.7;
        } else if (nilai.equalsIgnoreCase("c+")) {
            return 2.3;
        } else if (nilai.equalsIgnoreCase("c")) {
            return 2.0;
        } else if (nilai.equalsIgnoreCase("c-")) {
            return 1.7;
        } else if (nilai.equalsIgnoreCase("d")) {
            return 1.0;
        }
        return 0.0;
    }

    public float ipkCounter(ArrayList<MataKuliah> data) {
        float total = 0;
        int skstotal = 0;
        for (MataKuliah x : data) {
            String nilai = x.getHuruf(x.getnilai());
            int sksMatkul = x.getSks();

            if (nilai.equalsIgnoreCase("a")) {
                total += 4 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("a-")) {
                total += 3.7 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("b+")) {
                total += 3.3 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("b")) {
                total += 3 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("b-")) {
                total += 2.7 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("c+")) {
                total += 2.3 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("c")) {
                total += 2.0 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("c-")) {
                total += 1.7 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("d")) {
                total += 1 * sksMatkul;
                skstotal += sksMatkul;
            } else if (nilai.equalsIgnoreCase("e")) {
                total += 0 * sksMatkul;
                skstotal += sksMatkul;
            }

        }
        return total / skstotal;
    }

    public static int sksCounter(float ipk) {
        if (ipk < 2.00)
            return 12;
        else if (2.00 <= ipk && ipk < 2.50)
            return 15;
        else if (2.50 <= ipk && ipk < 3.00)
            return 18;
        else if (3.00 <= ipk && ipk < 3.50)
            return 21;
        else
            return 24;
    }

    public List<String> getMatkul() {
        return matkul;
    }

    public void setMatkul(ArrayList<String> matkul) {
        this.matkul = matkul;
    }

    public static float getIpk() {
        return ipk;
    }

    public void setIpk(float ipk) {
        Database.ipk = ipk;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public static ArrayList<MataKuliah> getDatabaseMatkulUser() {
        return databaseMatkulUser;
    }
    public ArrayList<MataKuliah> getDatabaseMatkul() {
        return databaseMatkul;
    }
}
