package miniproject.demo;

public class SIAK extends MataKuliah {
    public SIAK(double nilai, boolean lulus) {
        super("Sistem Informasi Akuntansi & Keuangan", nilai, new MataKuliah[]{new Adbis(0, true)}, lulus, 3, "SI");
    }
}