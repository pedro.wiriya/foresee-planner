//Code made by Pedro Wiriya in collaboration with Adrian Chandra Rachman
package miniproject.demo;

public abstract class MataKuliah {

    private String nama;
    private double nilai;
    private MataKuliah[] prasyarat;
    private boolean lulus;
    private int sks;
    private String list;
    
    public MataKuliah(String nama, double nilai, MataKuliah[] prasyarat, boolean lulus, int sks, String list) {
        this.nama = nama;
        this.nilai = nilai;
        this.prasyarat = prasyarat;
        this.lulus = lulus;
        this.sks = sks;
        this.list = list;
    }

    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getnilai() {
        return nilai;
    }
    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public int getSks() {
        return sks;
    }
    public void setSks(int sks) {
        this.sks = sks;
    }
    public MataKuliah[] getPrasyarat() {
        return prasyarat;
    }

    public boolean getLulus() {
        return lulus;
    }

    public String getList() {
        return list;
    }
    public void setList(String list) {
        this.list = list;
    }
    
    public void setLulus(boolean lulus) {
        this.lulus = lulus;
    }
    public String getHuruf(double nilai) {
        if (nilai == 4.0) {
            return "A";
        } else if (nilai == 3.7) {
            return "A-";
        } else if (nilai == 3.3) {
            return "B+";
        } else if (nilai == 3.0) {
            return "B";
        } else if (nilai == 2.7) {
            return "B-";
        } else if (nilai == 2.3) {
            return "C+";
        } else if (nilai == 2.0) {
            return "C";
        } else if (nilai == 1.7) {
            return "C-";
        } else if (nilai == 1.0) {
            return "D";
        }
        return "E";
    }

    @Override
    public String toString() {
        return this.nama;
    }
    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (!(o instanceof MataKuliah))
            return false;
        MataKuliah x = (MataKuliah) o;
        return this.getNama().equalsIgnoreCase(x.getNama());
    }
    
}