package miniproject.demo;

import java.util.*;

public class BeforeHTML {
    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        System.out.println("Selamat datang di Foresee Planner. Silahkan pilih angka untuk menu di bawah ini");
        System.out.println("1. Input");
        System.out.println("2. IPK cek");
        System.out.println("3. SKS cek");
        System.out.println("4. Matkul Pilihan");
        System.out.println("0. Keluar");
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        if (input.equalsIgnoreCase("1")) {
            input();
        } else if (input.equalsIgnoreCase("2")) {
            ipkCounter();
        } else if (input.equalsIgnoreCase("3")) {
            sksCounter();
        } else if (input.equalsIgnoreCase("4")) {
            matkulPilihan();
        }else if (input.equalsIgnoreCase("0")) {
            System.out.println("Terima Kasih");
        }
        scan.close();
    }

    public static void input() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukkan mata kuliah: ");
        String nama = scan.nextLine();
        System.out.println("Masukkan nilai dalam alphabethic: ");
        String nilai = scan.nextLine();
        String[] data = {nama, nilai}; 
        if (data.length != 0) {
            Database database= new Database();
            database.addUserMatkul(data);
            System.out.println(String.format("Anda memasukkan %d mata kuliah", Database.getDatabaseMatkulUser().size()));
        } 
        System.out.println("Kembali ke menu? Y/N");
        String pilihan = scan.nextLine();
        if (pilihan.equalsIgnoreCase("y")) {
            menu();
        }
        System.out.println("Terima Kasih");
        scan.close();
    }

    public static void ipkCounter() {
        Database data = new Database();
        System.out.println(String.format("IPK anda : %f", data.ipkCounter(Database.getDatabaseMatkulUser())));
        Scanner scan = new Scanner(System.in);
        System.out.println("Kembali ke menu? Y/N");
        String pilihan = scan.nextLine();
        if (pilihan.equalsIgnoreCase("y")) {
            menu();
        }
        System.out.println("Terima Kasih");
        scan.close();
    }

    public static void sksCounter() {
        Database data = new Database();
        System.out.println(String.format("SKS semester depan yang dapat diambil : %d", data.sksCounter(data.ipkCounter(Database.getDatabaseMatkulUser()))));
        System.out.println("Kembali ke menu? Y/N");
        Scanner scan = new Scanner(System.in);
        String pilihan = scan.nextLine();
        if (pilihan.equalsIgnoreCase("y")) {
            menu();
        }
        System.out.println("Terima Kasih"); 
        scan.close();
    }
    public static void matkulPilihan() {
        Database data = new Database();
        ArrayList<MataKuliah> ilkom = data.getIlkom();
        ArrayList<MataKuliah> si = data.getSi();
        System.out.println(String.format("Matkul input : %d", Database.getDatabaseMatkulUser().size()));
        for(MataKuliah x :Database.getDatabaseMatkulUser()) {
            if (ilkom.contains(x)) {
                ilkom.remove(x);
            } else if (si.contains(x)) {
                si.remove(x);
            }
        }
        System.out.println("Matkul ilkom : ");
        for (MataKuliah il : ilkom) {
            System.out.println(il.getNama());
        }
        System.out.println("Matkul si : ");
        for (MataKuliah s : si) {
            System.out.println(s.getNama());
        }
        Scanner scan = new Scanner(System.in);
        String pilihan = scan.nextLine();
        if (pilihan.equalsIgnoreCase("y")) {
            menu();
        }
        System.out.println("Terima Kasih"); 
        scan.close();

    }
}