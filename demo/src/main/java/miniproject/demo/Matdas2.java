
//Code made by Adrian Chandra Rachman - 1906399575
package miniproject.demo;

public class Matdas2 extends MataKuliah {

    public Matdas2(double nilai, boolean lulus) {
        super("Matematika Dasar 2", nilai, new MataKuliah[]{new MatDas(0, true)} , lulus, 3, "Ilkom");
    }
}