package miniproject.demo;

public class PemLan extends MataKuliah {
    public PemLan(double nilai, boolean lulus) {
        super("Pemrograman Lanjut", nilai, new MataKuliah[]{new PPW(0, true)}, lulus, 4, "Ilkom");
    }
}