//Code made by Adrian Chandra Rachman - 1906399575

package miniproject.demo;

public class SDA extends MataKuliah {

    public SDA(double nilai, boolean lulus) {
        super("Struktur Data & Algoritma ", nilai, new MataKuliah[]{new DDP2(0, true)}, lulus, 4, "SI/Ilkom");
    }
}