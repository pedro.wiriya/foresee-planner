//Code made by Adrian Chandra Rachman -1906399575

package miniproject.demo;

public class PPSI extends MataKuliah {

    public PPSI(double nilai, boolean lulus) {
        super("Prinsip-Prinsip Sistem Informasi", nilai, new MataKuliah[]{}, lulus, 3, "SI");
    }
}
