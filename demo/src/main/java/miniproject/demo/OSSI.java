package miniproject.demo;
public class OSSI extends MataKuliah {
    public OSSI(double nilai, boolean lulus) {
        super("Sistem Operasi", nilai, new MataKuliah[]{new DDAK(0, true)}, lulus, 4, "SI");
    }
}