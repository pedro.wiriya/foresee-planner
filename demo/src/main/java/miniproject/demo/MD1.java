package miniproject.demo;

public class MD1 extends MataKuliah {

    public MD1(double nilai, boolean lulus) {
        super("Matematika Diskret 1", nilai, new MataKuliah[]{}, lulus, 3, "SI/Ilkom");
    }
}