//Code made by Adrian Chandra Rachman - 1906399575
package miniproject.demo;

public class MPKTA extends MataKuliah {

    public MPKTA(double nilai, boolean lulus) {
        super("MPK Terintegrasi A", nilai, new MataKuliah[]{}, lulus, 6, "SI/Ilkom");
    }
}