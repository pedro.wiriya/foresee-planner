package miniproject.demo;

public class DDP1 extends MataKuliah {
    public DDP1(double nilai, boolean lulus) {
        super("Dasar-Dasar Pemrograman 1", nilai, new MataKuliah[]{}, lulus, 4, "SI/Ilkom");
    }
}