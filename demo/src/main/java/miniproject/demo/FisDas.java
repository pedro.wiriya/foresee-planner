package miniproject.demo;

public class FisDas extends MataKuliah {
    
    public FisDas(double nilai, boolean lulus) {
        super("Fisika Dasar", nilai, new MataKuliah[]{}, lulus, 3, "SI/Ilkom");
    }
}