//Code made by Adrian Chandra Rachman - 1906399575
package miniproject.demo;

public class Alin extends MataKuliah{
    public Alin(double nilai, boolean lulus) {
        super("Aljabar Liniear", nilai, new MataKuliah[]{new MatDas(0, false)} , lulus, 3, "SI/Ilkom");
    }
}
