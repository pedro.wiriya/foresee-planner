package miniproject.demo;

import java.util.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class MainController {
    
    Database data = new Database();

    @GetMapping("/index")
    public String home() {
        return "index";
    }
    @GetMapping("/final")
    public String finalForm(Model model) {
        ArrayList<MataKuliah> ilkom = data.getIlkom();
        ArrayList<MataKuliah> si = data.getSi();
        ArrayList<MataKuliah> user = Database.databaseMatkulUser;
        for (MataKuliah x :  user) {
            ilkom.remove(x);
            si.remove(x);
        }
        model.addAttribute("si", si);
        model.addAttribute("ilkom", ilkom);
        return "Final";
    }
    @GetMapping("/ipkCounter")
    public String ipkcountafter(Model model) {
        model.addAttribute("input", new Input());
        return "ipkCounter";
    }
    @PostMapping("/ipkCounter")
    public String ipkcountafter1(@ModelAttribute Input input, Model model) {
        if (input.getNama() != null && input.getNilai() != null) {
            String[] in = {input.getNama(), input.getNilai()};
            data.addUserMatkul(in);
        }
        int panjang = Database.databaseMatkulUser.size();
        String total = Integer.toString(panjang);
        model.addAttribute("total", total);
        return "ipkCounterAftermath";
    }
    @GetMapping("/ipkReveal")
    public String ipkreveal(Model model) {
        ArrayList<Output> out = new ArrayList<Output>();
        for (MataKuliah x : Database.databaseMatkulUser) {
            out.add(new Output(x.getNama(), x.getSks(), x.getHuruf(x.getnilai())));
        }
        Float i = data.ipkCounter(Database.getDatabaseMatkulUser());
        data.setIpk(i);;
        String ipk = String.format("%.2f", i);
        model.addAttribute("out", out);
        model.addAttribute("ipk", ipk);
        return "ipkReveal";
    }
    @GetMapping("/sksCounter")
    public String sksCounter(Model model) {
        model.addAttribute("ipkk", new IPK());
        return "SKSCounter";
    }
    @PostMapping("/sksCounter")
    public String sksCounterAfter(@ModelAttribute IPK ipk, Model model) {
        int x = data.sksCounter(ipk.getIpk());
        String ipkk = String.format("%.2f", ipk.getIpk());
        String sks = String.format("%d", x);
        model.addAttribute("sks", sks);
        model.addAttribute("ipk", ipkk);
        return "SKSCounterAfter";
    }
    @GetMapping("/sksCounters")
    public String sksFromIPKreveal(Model model) {
        String ipk = String.format("%.2f", Database.getIpk());
        int x = data.sksCounter(Database.getIpk());
        String sks = String.format("%d", x);
        model.addAttribute("sks", sks);
        model.addAttribute("ipk", ipk);
        return "sksFromIPKReveal";
    }
    
    @GetMapping("/about-us")
    public String aboutUs() {
        return "aboutus";
    }

}