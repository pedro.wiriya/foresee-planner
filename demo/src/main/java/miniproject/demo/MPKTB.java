package miniproject.demo;
public class MPKTB extends MataKuliah {

    public MPKTB(double nilai, boolean lulus){
        super("MPK Terintegrasi B", nilai, new MataKuliah[]{}, lulus, 6, "SI/Ilkom");
    }
}