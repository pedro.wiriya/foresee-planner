//Code made by Adrian Chandra Rachman - 1906399575

package miniproject.demo;

public class DDP2 extends MataKuliah {

    public DDP2(double nilai, boolean lulus) {
        super("Dasar-Dasar Pemrograman 2", nilai, new MataKuliah[]{new DDP1(0, true)} , lulus, 4, "SI/Ilkom");
    }
}