package miniproject.demo;

public class SSP extends MataKuliah {
    public SSP(double nilai, boolean lulus) {
        super("Sistem-Sistem Perusahaan", nilai, new MataKuliah[]{new Adbis(0, true)}, lulus, 3, "SI");
    }
}