package miniproject.demo;

public class StatProb extends MataKuliah{
    public StatProb(double nilai, boolean lulus) {
        super("Statistika & Probabilitas", nilai, new MataKuliah[]{new MatDas(0, true), new MD1(0, true)}, lulus, 3, "SI/Ilkom");
    }
}