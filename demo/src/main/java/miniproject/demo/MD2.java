//Code made by Adrian Chandra Rachman - 1906399575
package miniproject.demo;
public class MD2 extends MataKuliah {

    public MD2(double nilai, boolean lulus) {
        super("Matematika Diskret 2", nilai, new MataKuliah[]{}, lulus, 3, "SI/Ilkom");
    }
}