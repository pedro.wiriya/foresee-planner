package miniproject.demo;

public class BasDat extends MataKuliah {
    public BasDat(double nilai, boolean lulus) {
        super("Basis Data", nilai, new MataKuliah[]{new DDP2(0, true)}, lulus, 4, "SI/Ilkom");  
    }
}