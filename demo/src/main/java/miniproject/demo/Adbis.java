//Code made by Adrian Chandra Rachman - 1906399575
package miniproject.demo;

public class Adbis extends MataKuliah {

    public Adbis(double nilai, boolean lulus) {
        super("Administrasi Bisnis", nilai, new MataKuliah[]{} , lulus, 3, "SI");
    }
}