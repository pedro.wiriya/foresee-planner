package miniproject.demo;

public class Output {
    private String nama;
    private int sks;
    private String nilai;

    public Output(String nama, int sks, String nilai) {
        this.nama = nama;
        this.sks = sks;
        this.nilai = nilai;
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNilai() {
        return nilai;
    }
    public void setNilai(String nilai) {
        this.nilai = nilai;
    }
    public int getSks() {
        return sks;
    }
    public void setSks(int sks) {
        this.sks = sks;
    }
}